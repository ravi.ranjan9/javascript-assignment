Array.prototype.swap = function (x, y) {
  let b = this[x];
  this[x] = this[y];
  this[y] = b;
  return this;
};

class Person {
  constructor(name, age, salary, sex) {
    this.name = name;
    this.age = age;
    this.salary = salary;
    this.sex = sex;
  }

  static partitionAsc(arr, param, l, h) {
    const pivot = arr[h][param];

    let i = l - 1;
    for (let j = l; j < h; j++) {
      if (arr[j][param] < pivot) {
        i++;
        arr.swap(i, j);
      }
    }

    arr.swap(i + 1, h);

    return i + 1;
  }

  static partitionDesc(arr, param, l, h) {
    const pivot = arr[h][param];
    let i = l - 1;
    for (let j = l; j < h; j++) {
      if (arr[j][param] > pivot) {
        i++;
        arr.swap(i, j);
      }
    }

    arr.swap(i + 1, h);

    return i + 1;
  }

  static quickSort(array, param, order, l, h) {
    if (l < h) {
      let p =
        order === 'asc'
          ? this.partitionAsc(array, param, l, h)
          : this.partitionDesc(array, param, l, h);
      this.quickSort(array, param, order, l, p - 1);
      this.quickSort(array, param, order, p + 1, h);
    }
  }

  static sort(arr, param, order) {
    let newArr = arr.slice(0);
    this.quickSort(newArr, param, order, 0, arr.length - 1);
    return newArr;
  }
}

const person1 = new Person('jeff', 22, 25000, 'male');
const person2 = new Person('bill', 28, 35800, 'male');
const person3 = new Person('jennifer', 21, 26000, 'female');
const person4 = new Person('vicky', 34, 98500, 'male');
const person5 = new Person('may', 29, 47500, 'female');

const persons = [person1, person2, person3, person4, person5];

console.log(Person.sort(persons, 'age', 'asc'));
console.log(Person.sort(persons, 'age', 'desc'));
console.log(Person.sort(persons, 'name', 'desc'));
console.log(Person.sort(persons, 'salary', 'desc'));
